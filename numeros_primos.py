#Programa para saber si un número es primo o no.

#Autora= "Martha Cango"
#Email= "martha.cango@unl.edu.ec"

while 1:
    try:
        num = int(input("Ingrese un número para saber si es primo o no: "))


        def numero_primo(num):
            divisor = 2
            while num > divisor:
                if num % divisor == 0:
                    print("No es un número primo")
                    break
                elif num % divisor != 0:
                    divisor = divisor + 1
            if num == divisor:
                print("Es un número primo")
            return num


        numero_primo(num)

    except:
        print("Entrada inválida")
